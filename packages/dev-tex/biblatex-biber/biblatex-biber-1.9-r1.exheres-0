# Copyright 2013 Thomas Witt <thomas@thwitt.de>
# Distributed under the terms of the GNU General Public License v2
#    Based on the biber-1.6.ebuild which is
#    Copyright 1999-2013 Gentoo Foundation

require perl-module

SUMMARY="Biber is a BibTeX replacement for users of biblatex"
DESCRIPTION="
Biber is conceptually a BibTeX replacement for Biblatex. It is written in Perl
with the aim of providing a customised and sophisticated data preparation
backend for Biblatex. Functionally, Biber offers a superset of BibTeX’s
capabilities but is tightly coupled with Biblatex and cannot be used as a
stand-alone tool with standard .bst styles. Biber’s primary role is to support
Biblatex.
"
HOMEPAGE="http://biblatex-biber.sourceforge.net/"
DOWNLOADS="mirror://sourceforge/project/${PN}/${PN}/${PV}/${PN}.tar.gz -> ${PNV}.tar.gz"

SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/perl:=[>=5.16.0]
        dev-perl/autovivification
        dev-perl/Business-ISBN
        dev-perl/Business-ISSN
        dev-perl/Business-ISMN
        dev-perl/Data-Compare
        dev-perl/Data-Dump
        dev-perl/Data-Diver
        dev-perl/Date-Simple
        dev-perl/Encode-EUCJPASCII
        dev-perl/Encode-HanExtra
        dev-perl/Encode-JIS2K
        dev-perl/File-Slurp
        dev-perl/IPC-Run3
        dev-perl/libwww-perl
        dev-perl/List-AllUtils
        dev-perl/List-MoreUtils
        dev-perl/Log-Log4perl
        dev-perl/MIME-Charset
        dev-perl/Mozilla-CA[>=20130114]
        dev-perl/Readonly
        dev-perl/Readonly-XS
        dev-perl/Regexp-Common
        dev-perl/Text-BibTeX
        dev-perl/Unicode-LineBreak
        dev-perl/URI
        dev-perl/XML-LibXML
        dev-perl/XML-LibXML-Simple
        dev-perl/XML-LibXSLT
    run:
        dev-tex/biblatex[~2.9]
    test:
        dev-texlive/texlive-basic
        dev-perl/File-Which
        dev-perl/XML-Writer
"

BUGS_TO="pyromaniac@thwitt.de"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/caf6348967d07fa9512e9288b8c0b3d34682ec37.patch  )

src_test() {
    unset http_proxy https_proxy ftp_proxy
    perl-module_src_test
}

