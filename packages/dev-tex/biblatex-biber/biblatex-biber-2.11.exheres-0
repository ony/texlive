# Copyright 2013-2015 Thomas Witt
# Distributed under the terms of the GNU General Public License v2
#    Based on the biber-1.6.ebuild which is
#    Copyright 1999-2013 Gentoo Foundation

require perl-module [ buildsystem=module-build ]
require github [ user=plk pn=biber tag=v${PV} ]

SUMMARY="Biber is a BibTeX replacement for users of biblatex"
DESCRIPTION="
Biber is conceptually a BibTeX replacement for Biblatex. It is written in Perl
with the aim of providing a customised and sophisticated data preparation
backend for Biblatex. Functionally, Biber offers a superset of BibTeX’s
capabilities but is tightly coupled with Biblatex and cannot be used as a
stand-alone tool with standard .bst styles. Biber’s primary role is to support
Biblatex.
"
HOMEPAGE+=" http://biblatex-biber.sourceforge.net/"

SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/perl:=[>=5.28.0] [[ note = [ Unicode-Collate >=1.25 ] ]]
        dev-perl/autovivification
        dev-perl/Business-ISBN
        dev-perl/Business-ISMN
        dev-perl/Business-ISSN
        dev-perl/Class-Accessor
        dev-perl/Data-Compare
        dev-perl/Data-Dump
        dev-perl/Data-Uniqid
        dev-perl/Date-Simple
        dev-perl/DateTime-Calendar-Julian
        dev-perl/DateTime-Format-Builder
        dev-perl/Encode-EUCJPASCII
        dev-perl/Encode-HanExtra
        dev-perl/Encode-JIS2K
        dev-perl/File-Slurper
        dev-perl/IPC-Run3
        dev-perl/libwww-perl
        dev-perl/Lingua-Translit[>=0.28]
        dev-perl/List-AllUtils
        dev-perl/List-MoreUtils
        dev-perl/List-MoreUtils-XS
        dev-perl/Log-Log4perl
        dev-perl/LWP-Protocol-https
        dev-perl/Mozilla-CA[>=20160104]
        dev-perl/PerlIO-utf8_strict
        dev-perl/Regexp-Common
        dev-perl/Sort-Key
        dev-perl/Text-BibTeX[>=0.85]
        dev-perl/Text-CSV
        dev-perl/Text-CSV_XS
        dev-perl/Text-Roman
        dev-perl/Unicode-LineBreak[>=2016.003]
        dev-perl/URI
        dev-perl/XML-LibXML[>=1.70]
        dev-perl/XML-LibXML-Simple
        dev-perl/XML-LibXSLT
        dev-perl/XML-Writer
    run:
        dev-tex/biblatex[~3.11]
    test:
        dev-perl/Test-Differences
        dev-perl/Test-Pod
        dev-perl/Test-Pod-Coverage
        dev-perl/File-Which
        dev-texlive/texlive-basic
"

BUGS_TO="pyromaniac@exherbo.org"

src_test() {
    unset http_proxy https_proxy ftp_proxy
    perl-module_src_test
}

