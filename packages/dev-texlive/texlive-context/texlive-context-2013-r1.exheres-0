# Copyright 2013 Bo Ørsted Andresen
# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2
# Based in part upon the Gentoo texlive ebuilds which are:
#     Copyright 1999-2013 Gentoo Foundation

# This exheres is generated. If you need to change it make sure to get the
# source fixed too.

TEXLIVE_MODULE_CONTENTS="
jmn context-account context-algorithmic context-bnf context-chromato context-construction-plan
context-cyrillicnumbers context-degrade context-filter context-fixme context-french
context-fullpage context-games context-gantt context-gnuplot context-letter context-lettrine
context-lilypond context-mathsets context-notes-zh-cn context-rst context-ruby context-simplefonts
context-simpleslides context-transliterator context-typearea context-typescripts context-vim
collection-context
"
TEXLIVE_MODULE_DOC_CONTENTS="context-account.doc context-bnf.doc context-chromato.doc context-construction-plan.doc
context-cyrillicnumbers.doc context-degrade.doc context-filter.doc context-french.doc
context-fullpage.doc context-games.doc context-gantt.doc context-gnuplot.doc context-letter.doc
context-lettrine.doc context-lilypond.doc context-mathsets.doc context-notes-zh-cn.doc
context-rst.doc context-ruby.doc context-simplefonts.doc context-simpleslides.doc
context-transliterator.doc context-typearea.doc context-typescripts.doc context-vim.doc"
TEXLIVE_MODULE_SRC_CONTENTS=""
require texlive-module
SUMMARY="TeXLive ConTeXt and packages"

LICENCES="BSD GPL-1 GPL-2 public-domain TeX-other-free"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""
DEPENDENCIES="
    build+run:
        dev-texlive/texlive-basic[>=2013]
        dev-texlive/texlive-latex[>=2010]
        app-text/texlive-core[>=2010][xetex]
        dev-texlive/texlive-metapost[>=2010]
    run:
        dev-lang/ruby:*
"
#DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/luacnfspec-2013.patch )

#TL_CONTEXT_UNIX_STUBS=( context ctxtools luatools metatex mtxrun mtxworks pstopdf texexec texmfstart )
#TEXLIVE_MODULE_BINSCRIPTS="${TL_CONTEXT_UNIX_STUBS[*]/#/texmf-dist/scripts/context/stubs/unix/}"

# This small hack is needed in order to have a sane upgrade path:
# the new TeX Live 2009 metapost produces this file but it is not recorded in
# any package; when running fmtutil (like texmf-update does) this file will be
# created and cause collisions.

pkg_setup() {
    if [[ -f ${ROOT}/var/lib/texmf/web2c/metapost/metafun.log ]]; then
        echo "Removing ${ROOT}/var/lib/texmf/web2c/metapost/metafun.log"
        edo rm "${ROOT}"/var/lib/texmf/web2c/metapost/metafun.log
    fi
    default
}

# These come without +x bit set...
src_prepare() {
    #edo chmod +x texmf-dist/scripts/context/ruby/ctxtools.rb \
        #texmf-dist/scripts/context/ruby/pstopdf.rb \
        #texmf-dist/scripts/context/ruby/rlxtools.rb
        # No need to install this exe
    edo rm -rf texmf-dist/scripts/context/stubs/mswin
    texlive-module_src_prepare
}

TL_MODULE_INFORMATION="For using ConTeXt mkII simply use 'texexec' to generate
your documents.
If you plan to use mkIV and its 'context' command to generate your documents,
you have to run 'luatools --generate' as normal user before first use.

More information and advanced options on:
http://wiki.contextgarden.net/TeX_Live_2011"

