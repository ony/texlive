# Copyright 2011 Ingmar Vanhassel
# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2
# Based in part upon kpathsea-6.0.0_p20100722.ebuild from Gentoo, which is:
# Copyright 1999-2010 Gentoo Foundation

require texlive-common

SUMMARY="A library to do path searching, mainly for TeX"
HOMEPAGE="http://tug.org/kpathsea/"
DOWNLOADS="
    mirror://ctan/systems/texlive/Source/texlive-${PV#*_p}-source.tar.xz
    http://dev.exherbo.org/distfiles/texlive/2013/texlive-module-${PN}-2013.tar.xz
    doc? ( http://dev.exherbo.org/distfiles/texlive/2013/texlive-module-${PN}.doc-2013.tar.xz )
    http://dev.exherbo.org/distfiles/texlive/2013/kpathsea-texmf.d-5.tar.xz
"

LICENCES="|| ( LGPL-3 LGPL-2.1 )"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="doc source static"

DEPENDENCIES="
    build+run:
        !app-text/texlive-core[<2013]
"

BUGS_TO="pyromaniac@thwitt.de"

require texlive-common

WORK=${WORKBASE}/texlive-${PV#*_p}-source/texk/${PN}

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( static )

src_prepare() {
    edo cd "${WORKBASE}/texlive-${PV#*_p}-source"
    default
}

src_install() {
    default

    # The default configuration expects it to be world writable, Gentoo #266680
    keepdir /var/cache/fonts/
    edo chmod 1777 "${IMAGE}"/var/cache/fonts

    keepdir /etc/texmf/{fmtutil.d,texmf.d}
    insinto /etc/texmf/texmf.d/
    doins "${WORKBASE}"/texmf.d/*.cnf

    edo rm -f "${WORK}"/texmf-dist/web2c/texmf.cnf
    edo rm -f "${WORK}"/texmf-dist/web2c/fmtutil.cnf

    keepdir /var/lib/texmf

    # texmf-update fails if this dir does not exist
    keepdir /etc/texmf/web2c/

    # Install texmf tree
    insinto /usr/share/
    edo cp -pPR "${WORKBASE}"/texmf-dist "${IMAGE}/usr/share"

    if optionq source; then
        edo cp -pPR "${WORKBASE}"/tlpkg "${IMAGE}/usr/share"
    fi

    dosym /etc/texmf/web2c/fmtutil.cnf /usr/share/texmf-dist/web2c/fmtutil.cnf
    dosym /etc/texmf/web2c/texmf.cnf /usr/share/texmf-dist/web2c/texmf.cnf

    dobin "${FILES}"/texmf-update

    emagicdocs
}

pkg_postrm() {
    etexmf-update
}

pkg_postinst() {
    etexmf-update
}

