# Copyright 2010, 2011 Ingmar Vanhassel
# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2
# Based in parts upon the texlive-core-2013.ebuild which is
#   Copyright 1999-2013 Gentoo Foundation

require autotools [ supported_autoconf=[ none ] supported_automake=[ 1.13 ] ] texlive-common

SUMMARY="The TeXlive distribution"
HOMEPAGE="http://tug.org/texlive"
TL_PN="texlive"
TL_PV="20140523"
DOWNLOADS="mirror://ctan/systems/${TL_PN}/Source/${TL_PN}-${TL_PV}-source.tar.xz"

LICENCES="as-is GPL-2 GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="cjk doc source X xetex"

TL_CORE_BINEXTRA_MODULES=(
    a2ping adhocfilelist asymptote bundledoc ctanify ctanupload ctie cweb
    de-macro dtl dtxgen dvi2tty dviasm dvicopy dvidvi dviljk dvipos findhyph
    fragmaster hyphenex installfont lacheck latex2man latexfileversion
    latexpand ltxfileinfo listings-ext match_parens mkjobtexmf
    patgen pdfcrop pdftools pfarrei pkfix pkfix-helper purifyeps seetexk
    sty2dtx synctex texcount texdef texdiff texdirflatten texdoc
    texliveonfly texloganalyser texware tie tpic2pdftex
    typeoutfileinfo web collection-binextra
    )
TL_CORE_BINEXTRA_DOC_MODULES=(
    a2ping.doc adhocfilelist.doc asymptote.doc bundledoc.doc ctanify.doc
    ctanupload.doc ctie.doc cweb.doc de-macro.doc dtxgen.doc dvi2tty.doc
    dvicopy.doc dviljk.doc dvipos.doc findhyph.doc fragmaster.doc
    installfont.doc latex2man.doc latexfileversion.doc latexpand.doc
    ltxfileinfo.doc listings-ext.doc match_parens.doc mkjobtexmf.doc patgen.doc
    pdfcrop.doc pdftools.doc pfarrei.doc pkfix.doc pkfix-helper.doc
    purifyeps.doc sty2dtx.doc synctex.doc texcount.doc texdef.doc texdiff.doc
    texdirflatten.doc texdoc.doc texliveonfly.doc texloganalyser.doc texware.doc
    tie.doc tpic2pdftex.doc typeoutfileinfo web.doc
    )
TL_CORE_BINEXTRA_SRC_MODULES=(
    adhocfilelist.source hyphenex.source listings-ext.source mkjobtexmf.source
    pfarrei.source texdef.source
    )

TL_CORE_EXTRA_MODULES=(
    tetex hyphen-base texconfig gsftopk texlive.infra ${TL_CORE_BINEXTRA_MODULES[@]}
    )
TL_CORE_EXTRA_DOC_MODULES=(
    tetex.doc texconfig.doc gsftopk.doc texlive.infra.doc ${TL_CORE_BINEXTRA_DOC_MODULES[@]}
    )
TL_CORE_EXTRA_SRC_MODULES=( ${TL_CORE_BINEXTRA_SRC_MODULES[@]} )

for i in ${TL_CORE_EXTRA_MODULES[@]}; do
    DOWNLOADS+=" http://dev.exherbo.org/distfiles/texlive/${PV}/texlive-module-${i}-${PV}.tar.xz"
done

DOWNLOADS+=" doc? ("
for i in ${TL_CORE_EXTRA_MODULES[@]}; do
    DOWNLOADS+=" http://dev.exherbo.org/distfiles/texlive/${PV}/texlive-module-${i}-${PV}.tar.xz"
done
DOWNLOADS+=" ) source? ("
for i in ${TL_CORE_EXTRA_MODULES[@]}; do
    DOWNLOADS+=" http://dev.exherbo.org/distfiles/texlive/${PV}/texlive-module-${i}-${PV}.tar.xz"
done
DOWNLOADS+=" )"

REMOTE_IDS="freecode:${TL_PN}"


RESTRICT="test"

DEPENDENCIES="
    build:
        app-arch/xz
        virtual/pkg-config
        sys-apps/ed
        sys-devel/flex
    build+run:
        app-text/dvipsk[>=5.994_p${TL_PV}]
        app-text/ghostscript [[ note = [ --with-system-libgs ] ]]
        app-text/poppler [[ note = [ --with-system-xpdf ] ]]
        dev-libs/icu:= [[ note = [ --with-system-icu ] ]]
        dev-libs/kpathsea[>=6.2.0_p${TL_PV}] [[ note = [ --with-system-kpathsea ] ]]
        dev-libs/ptexenc[>=1.3.2_p${TL_PV}] [[ note = [ --with-system-ptexenc ] ]]
        dev-libs/zziplib [[ note = [ --with-system-zziplib ] ]]
        dev-tex/luatex[>=0.76]
        media-libs/freetype:2 [[ note = [ --with-system-freetype2 ] ]]
        media-libs/gd [[ note = [ --with-system-gd ] ]]
        media-libs/libpng:= [[ note = [ --with-system-libpng ] ]]
        media-libs/t1lib [[ note = [ --with-system-t1lib ] ]]
        x11-libs/cairo [[ note = [ --with-system-cairo ] ]]
        x11-libs/pixman:1
        X? (
            x11-libs/libXmu
            x11-libs/libXp
            x11-libs/libXpm
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libXaw
            x11-libs/libXfont
        )
        xetex? (
            media-libs/fontconfig
            media-libs/freetype:2
            media-libs/TECkit [[ note = [ --with-system-teckit ] ]]
        )
        cjk? (
            dev-libs/ptexenc
        )
    recommendation:
        dev-tex/biblatex-biber [[ note = [ Replacement for BibTeX ] ]]
"

        #app-text/ps2pkm[>=1.5_p20120701]
        #dev-tex/bibtexu[>=3.71_p20130530]
            #media-gfx/graphite2
            #x11-libs/harfbuzz
        #xetex? ( app-text/xdvipdfmx[>=0.7.9_p20130530] ) ???

WORK="${WORKBASE}/${TL_PN}-${TL_PV}-build/"
#DEFAULT_SRC_PREPARE_PATCHES=( -p0 "${FILES}/texmfcnflua2013.patch" )
ECONF_SOURCE="${WORKBASE}/${TL_PN}-${TL_PV}-source/"

DEFAULT_SRC_COMPILE_PARAMS=( AR=${AR} )

pkg_pretend() {
    texlive_leftover_warning "${ROOT}"etc/texmf/web2c/updmap.cfg.d/updmap-hdr.cfg
}

src_unpack() {
    default
    edo mkdir "${WORK}"

    option doc || edo rm -rf texmf-dist/doc
}

src_prepare() {
    edo cd "${WORKBASE}"

    texlive-common_reloc_target

    edo cd "${ECONF_SOURCE}"

    # Find the prefixed strings command
    edo sed -e "s/if strings/if $(exhost --tool-prefix)strings/" \
            -i libs/cairo/m4/float.m4
    edo autoreconf libs/cairo

    default

    #FIXME GENTOO PATCH
    # Manually regenerate all Makefile.ams touched by our patches.
    # automake doesn't recurse, autoreconf takes too long.
    #edo autoreconf texk/cjkutils/ texk/dvipdfmx/ texk/dvipsk/ \
                   #texk/gsftopk/ texk/kpathsea/ texk/lcdf-typetools/ \
                   #texk/texlive/ texk/xdvik/ texk/xdvipdfmx/
}

src_configure() {
    local myconf=()

    myconf=(
        --with-banner-add="/Exherbo ${PNVR}"
        # If enabled, build for a TeX Live binary distribution as shipped by the TeX user groups
        --disable-native-texlive-build
        # Don’t install executables, binaries into platform dependent subdirectories of bindir, libdir
        --disable-multiplatform
        --enable-shared
        --disable-static
        # Terminate the build if a requested feature can’t be enabled due to missing dependencies
        --disable-missing

        # Fails for some subdirectories
        --hates=docdir

        # We don’t currently have freetype:1, this is only used by texk/ttf2pk, which we disable
        --with-system-{cairo,freetype2,gd,icu,kpathsea,libgs,pixman,libpng,ptexenc,poppler,t1lib,teckit,xpdf,zlib,zziplib}
        --without-system-{graphite2,harfbuzz}
        --with-freetype2-include=/usr/$(exhost --target)/include
        --with-kpathsea-includes=/usr/$(exhost --target)/include
        --with-petexenc-include=/usr/$(exhost --target)/include

        --without-freetype

        # Following features disabled temporarily until needed
        --disable-lcdf-typetools
        --disable-ttf2pk # requires freetype:1
        --disable-xindy

        --disable-luatex # separate package

        --disable-biber # separate package

        --disable-ps2eps
        --disable-dvipng
        --disable-dvipsk #separate package
        --disable-dvipdfmx
        --disable-chktex
        --disable-pdfopen
        --enable-ps2pkm
        --disable-detex
        --disable-ttf2pk
        --disable-ttf2pk2
        --disable-tex4htk
        --disable-cjkutils
        --disable-xdvik
        --disable-dvi2tty
        --disable-dvisvgm
        --disable-vlna

        # TODO system graphite2
        $(option_enable xetex)
        $(option_enable xetex xdvipdfmx)
        #$(option_with xetex graphite2)

        $(option_enable cjk ptex)
        $(option_enable cjk eptex)
        $(option_enable cjk uptex)
        $(option_enable cjk euptex)
        $(option_enable cjk mendexk)
        $(option_enable cjk makejvf)
        $(option_with cjk system-ptexenc)

        $(option_with X x)

        --disable-texdoctk
        --disable-dialog # sys-apps/dialog
        --disable-psutils # app-text/psutils
    )

    econf "${myconf[@]}"
}

src_install() {
    default

    dosym bibtex8 /usr/$(exhost --target)/bin/bibtex

    # FIXME: Comb through /usr/texmf*/scripts for cruft that shouldn't be installed
    # Get rid of tlmgr bits
    edo rm -rf "${IMAGE}"/usr/{$(exhost --target)/bin/tlmgr,texmf/scripts/texlive/}

    edo cp -pPR "${WORKBASE}/"texmf-dist "${IMAGE}"/usr/share/
    dodir /usr/share/tlpkg
    edo cp -pPR "${WORKBASE}/"tlpkg/TeXLive "${IMAGE}"/usr/share/tlpkg/

    dodir /etc/texmf/updmap.d
    edo mv "${IMAGE}/usr/share/texmf-dist/web2c/updmap-hdr.cfg" "${IMAGE}/etc/texmf/updmap.d"
    dodir /etc/texmf/fmtutil.d
    edo mv ${IMAGE}/usr/share/texmf-dist/web2c/fmtutil-hdr.cnf "${IMAGE}/etc/texmf/fmtutil.d"

    texlive-common_handle_config_files

    # autogenerated by etexmf-update
    edo rm "${IMAGE}"/usr/share/texmf-dist/web2c/updmap.cfg
    edo rm "${IMAGE}"/usr/share/texmf-dist/web2c/fmtutil.cnf
    edo rm "${IMAGE}"/usr/share/texmf-dist/tex/generic/config/language.dat
    edo rm "${IMAGE}"/usr/share/texmf-dist/tex/generic/config/language.dat.lua
    edo rm "${IMAGE}"/usr/share/texmf-dist/tex/generic/config/language.def

    # handled by dev-tex/glossaries
    nonfatal edo rm "${IMAGE}/usr/$(exhost --target)/bin/makeglossaries"
    # handled by dev-texlive/langgreek
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/mkgrkindex
    # handled by dev-texlive/langcyrillic
    nonfatal edo rm "${IMAGE}"/usr/share/texmf-dist/scripts/texlive/rubibtex.sh
    nonfatal edo rm "${IMAGE}"/usr/share/texmf-dist/scripts/texlive/rumakeindex.sh
    # handled by dev-texlive/texlive-fontutils
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/accfonts
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/dosepsbin
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/epstopdf
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/fontools
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/mf2pt1
    nonfatal edo rm "${IMAGE}"/usr/share/texmf-dist/scripts/texlive/fontinst.sh
    nonfatal edo rm "${IMAGE}"/usr/share/texmf-dist/scripts/texlive/ps2frag.sh
    nonfatal edo rm "${IMAGE}"/usr/share/texmf-dist/scripts/texlive/pslatex.sh
    # handled by dev-texlive/texlive-langindic
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/ebong
    # handled by dev-texlive/texlive-latex
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/context/perl
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/oberdiek
    # handled by dev-texlive/texlive-latexrecommended
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/thumbpdf
    # handled by dev-texlive/texlive-luatex
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/checkcites
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/lua2dox
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/luaotfload
    # handled by dev-texlive/texlive-pstricks
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/cachepic
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/epspdf
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/fig4latex
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/mathspic
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/mkpic
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/pedigree-perl
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/pst2pdf
    # handled by dev-texlive/texlive-latexextra
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/authorindex
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/exceltex
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/glossaries
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/pax
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/perltex
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/pst-pdf
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/splitindex
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/svn-multi
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/vpe
    # handled by dev-texlive/texlive-basic
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/dvips
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/dvipdfmx
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/texconfig
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/fonts
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/tex/generic
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/simpdftex
    nonfatal edo rm -r "${IMAGE}"/usr/share/tlpkg
    # handled by dev-texlive/texlive-science
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/ulqda
    # handled by dev-texlive/texlive-music
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/lilyglyphs
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/m-tx
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/musixtex
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/pmx{,chords}
    # handled by dev-texlive/texlive-games
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/rubik
    # handled by dev-texlive/texlive-bibtexextra
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/bibexport
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/multibibliography
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/urlbst
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/listbib
    # handled by dev-texlive/texlive-context
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/context

    keepdir /usr/share/texmf-dist/web2c
}

pkg_postinst() {
    etexmf-update
}

pkg_postrm() {
    if [[ -z ${REPLACED_BY_ID} ]]; then
        nonfatal edo rm "${ROOT}"/etc/texmf/web2c/updmap.cfg
        nonfatal edo rm "${ROOT}"/etc/texmf/web2c/fmtutil.cfg
    fi
}

